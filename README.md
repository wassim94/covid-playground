# COVID-Playground

## 1. First Goal: Hypothesis Test
**- H0: Corona Virus spread-speed and temperature are two independent _random variables_** (Hypothesis to be rejected)

**- H1: Corona Virus spread-speed and temperature are co-dependent.**

>  we basically look for enough statistical evidence to reject H0. 

## 2. tbd


---
**Resources**

* Kaggle dataset: https://www.kaggle.com/sudalairajkumar/novel-corona-virus-2019-dataset
* Kaggle SIR Model: https://www.kaggle.com/lisphilar/covid-19-data-with-sir-model
* Kaggle SIR Model(2): https://www.kaggle.com/saga21/covid-global-forecast-sir-model-ml-regressions
* HU SIR Model Intro: http://rocs.hu-berlin.de/corona/docs/forecast/model/


# Execution instructions:

## The easy way
Requirements:
* Having [docker](https://www.docker.com/get-started) and docker compose installed.

Instructions:

1. Open a terminal at this directory.
2. Execute `docker-compose up --build jupyter`
3. In your browser, open [localhost:1088](localhost:1088)

---

## Alternative
Requirements
* Having [Jupyter](https://jupyter.org) installed.

Instructions:

1. Open a terminal at this directory.
2. Install the dependencies in *requirements.txt* with conda or pip <br>
`python3 -m pip install -r requirements.txt`
3. Execute `jupyter notebook`
4. Open the link given by jupyter.

---
## Sharing Windows folders with containers
https://token2shell.com/howto/docker/sharing-windows-folders-with-containers/
